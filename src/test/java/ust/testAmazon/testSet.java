package ust.testAmazon;

import java.io.IOException;
import java.lang.reflect.Method;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Page.AddedToCart;
import Page.AllDepartmentCategories;
import Page.BestSeller;
import Page.Cart;
import Page.Homepage;
import Page.ProductAddToCart;
import Page.ProductDetails;
import Types.ActionButton;
import Utilities.ActionModule;
import Utilities.Data;
import Utilities.Reports;

public class testSet extends ActionModule {
	
	
	
	
	@BeforeClass
	public void beforeClass() throws IOException {
		Reports report = new Reports();	
		Data.setReport(report.extent);		
		Data.setTestSetData();    
		clearFilesInScreenshotFolder();
    
	}
	
	@AfterClass
	public void afterClass() {
		 
		 
	}
	
	@BeforeMethod
	public void beforeMethod(Method method) {
		//Data.currentTest = Data.report.createTest(method.getName());
		String testName = method.getName();	
		reportsCreateNewTest(testName);
		Data.setCurrentTestData(testName); // takes the test name and retrieves the value from test data map

	}
	@AfterMethod
	public void afterMethod() {
		
		testClosure();
	}
	

	@Test
	public void Case01() {
	
		LaunchUrl(Homepage.url);
		
		ClickOnHeaderTab(Homepage.bestSeller);		
		VerifyPageLoadText(BestSeller.HeadLine, "Amazon Bestsellers");	
		
		ClickOnBestSellerOptions(BestSeller.Electronics);
		VerifyPageLoadText(AllDepartmentCategories.ElectronicsHeader, "Electronics");
		
		ClickOnAllDepartmentOptions(AllDepartmentCategories.CameraNPhones);
		VerifyPageLoadText(AllDepartmentCategories.CameraNPhonesHeader, "Cameras & Photography");
		
		ClickOnAllDepartmentOptions(AllDepartmentCategories.DigitalCamera);
		VerifyPageLoadText(AllDepartmentCategories.DigitalCameraHeader, "Digital Cameras");
		
		ClickOnNthItemInDisplayedProductList(AllDepartmentCategories.displayedProducts, Data.CurrentTestData.get("productNumberToOpen"));
		VerifyPageLoadText(ProductDetails.loadText, "Buy Now");
		
		Data.setCurrentTestDataProperty("addedProductTitle",ProductDetails.Title.toString());
		Data.setCurrentTestDataProperty("finalPrice",ProductDetails.FinalPrice.toString());
			
//		ProductDetails.selectQuantity();
		SelectValueInDropdown("numberOfQuantity",ProductDetails.QntyDropdown);
		
	//	Data.CurrentTestData.get("numberOfQuantity")

		ClickOnActionButton(Data.getOpenProduct().AddToCartBTN);	
			
		AddedToCart.VerifyAddition();
		
		
		ClickOnHeaderTab(Homepage.cart);		
		VerifyPageLoadText(Cart.Header,"Shopping Cart");
		
		Cart.VerifyPresenceOfItem(Data.CurrentTestData.get("addedProductTitle"));			
		Cart.VerifyPriceSubTotalofItem();
				 
		
	}

	
	
}
