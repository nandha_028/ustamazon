package Utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import Page.ProductDetails;

public class Data {

	public static ProductDetails OpenedProduct ;
	public static HashMap<String, String> CurrentTestData ;
	public static HashMap<String, HashMap> TestSetData ;
	public static ProductDetails currentProductDetails;
	public static ExtentReports report = new ExtentReports();
	public static ExtentTest currentTest ; 

	static boolean dataset = false;
	
	
//	public static void setReportCurrentTest(ExtentTest test) {
//		currentTest = test;
//	}
	
	public static void setReport(ExtentReports rep) {
		report = rep;
	}
	public static HashMap<String,HashMap> getTestSetData() {
		return TestSetData;
	}
	public static  void setTestSetData() throws IOException{

		HashMap<String, HashMap> TestSetDetails = new HashMap<String,HashMap>(); ;		
		String file = System.getProperty("user.dir") +  "/testData.xlsx";
		XSSFWorkbook myExcelBook = new XSSFWorkbook(new FileInputStream(file));
		XSSFSheet myExcelSheet = myExcelBook.getSheet("TestData");

		int rowCount = myExcelSheet.getLastRowNum();
		XSSFRow row ;
		XSSFRow headerRows ;
		XSSFRow valueRow;

		for(int i = 0; i< rowCount; i++) {
			row = myExcelSheet.getRow(i);

			headerRows = myExcelSheet.getRow(i+1);
			valueRow = myExcelSheet.getRow(i+2);

			if(row.getCell(0).toString().contains("Case")) {
				HashMap <String, String > testDetails = new HashMap<String, String>();	
				int cellCountinRow = headerRows.getLastCellNum();
				for(int i1 = 0; i1<cellCountinRow; i1++ ) {
					if(!headerRows.getCell(i1).toString().isEmpty()) {
						if(valueRow.getCell(i1) != null) {
							testDetails.put(headerRows.getCell(i1).toString(), valueRow.getCell(i1).toString());
						}

					}
				}

				TestSetDetails.put(row.getCell(0).toString(), testDetails);
			}


		}
		TestSetData = TestSetDetails;
		System.out.println(TestSetDetails);
		myExcelBook.close();

	}

	public static void setCurrentTestData(String testName) {
		Iterator itr = TestSetData.entrySet().iterator();	
		while(itr.hasNext()) {
			Map.Entry<String, HashMap> entri = (Entry<String, HashMap>) itr.next();
			System.out.println(entri.getKey() + "is the key ");
			System.out.println(entri.getValue() + "is the Value");
			String key = entri.getKey();
			
			if(key.equalsIgnoreCase(testName)) {
				CurrentTestData = entri.getValue();
				break;
			}
		}
	}

	public static  ProductDetails getOpenProduct() {
		setOpenProduct();
		return OpenedProduct;
	}
	public static void setOpenProduct() {
		OpenedProduct = new ProductDetails();
	}
	public static void setCurrentTestDataProperty(String string, String string2) {
		
		Data.CurrentTestData.put(string, string2);
	}



}
