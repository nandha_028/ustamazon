package Utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelPOI {
	
	HashMap <String, HashMap > testSetDetails = new HashMap<String, HashMap>();
	
	public  void setTestData() throws IOException{
		
		String file = System.getProperty("user.dir") +  "/testData.xlsx";
        XSSFWorkbook myExcelBook = new XSSFWorkbook(new FileInputStream(file));
        XSSFSheet myExcelSheet = myExcelBook.getSheet("TestData");
      
        int rowCount = myExcelSheet.getLastRowNum();
        XSSFRow row ;
        XSSFRow headerRows ;
        XSSFRow valueRow;
        
       
        
        for(int i = 0; i< rowCount; i++) {
        	row = myExcelSheet.getRow(i);
        	
        	headerRows = myExcelSheet.getRow(i+1);
        	valueRow = myExcelSheet.getRow(i+2);
        	
        	if(row.getCell(0).toString().contains("Case")) {
        		HashMap <String, String > testDetails = new HashMap<String, String>();	
        		int cellCountinRow = headerRows.getLastCellNum();
        		for(int i1 = 0; i1<cellCountinRow; i1++ ) {
        			if(!headerRows.getCell(i1).toString().isEmpty()) {
        				testDetails.put(headerRows.getCell(i1).toString(), valueRow.getCell(i1).toString());
        			}
        		}
        		
        		testSetDetails.put(row.getCell(0).toString(), testDetails);
        	}
        	
        	
        }
       System.out.println(testSetDetails);
        myExcelBook.close();
        
    }
}
