package Utilities;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.Status;
import com.google.common.io.Files;

import Page.AddedToCart;
import Page.ProductDetails;
import Types.ActionButton;
import Types.AllDepartmentOptions;
import Types.BestSellerDisplayedProduct;
import Types.BestSellerOptions;
import Types.DisplayedText;
import Types.HeaderTab;
import Types.PageLoadText;
import Types.ProductList;
import Types.SelectDropdown;
import Types.URL;

public class ActionModule  {

	static Driver dr = new Driver();
public	static WebDriver driver = dr.getChromerDriver();
	static WebDriverWait wait20 = new WebDriverWait(driver, 20); 

	Actions action = new Actions(driver);
	static SoftAssert SA = new SoftAssert();

	protected void LaunchUrl(URL url) {
		driver.get(url.value) ;
	}

	protected void ClickOnHeaderTab(HeaderTab headerTab) {	
		clickAfterVisibleAndEnabled(headerTab.getElement());
	}

	public static void VerifyPageLoadText(PageLoadText pageLoadText, String ExpectedText) {
		WebElement E = pageLoadText.getElement();
		String ActualText= wait20.until(ExpectedConditions.visibilityOf(E)).getText();
		SA.assertEquals(ActualText, ExpectedText);
		
		
		if(ActualText.equalsIgnoreCase(ExpectedText)) {
		
			Data.currentTest.log(Status.INFO, ExpectedText + " Page is displayed succesfully", takeNGetScreenshot());
		}
		else{			
			Data.currentTest.log(Status.FAIL, ExpectedText + " Page is not displayed", takeNGetScreenshot());
		}
		
		
	}

	protected void ClickOnBestSellerOptions(BestSellerOptions options) {	
		clickAfterVisibleAndEnabled(options.getElement());		
	}

	protected void ClickOnAllDepartmentOptions(AllDepartmentOptions options) {
		clickAfterVisibleAndEnabled(options.getElement());	
	}

	protected void ClickOnNthItemInDisplayedProductList(ProductList displayedProducts, String string) {
		List<WebElement> elements = displayedProducts.getElementList();
		Double d = Double.parseDouble(string);

		WebElement E = elements.get(d.intValue()-1);
		BestSellerDisplayedProduct P = new BestSellerDisplayedProduct(E);
		
		addToCurrentTestData("SelectedProductTitle",P.Name);
		addToCurrentTestData("SelectedProductPrice",P.Price);
		
		clickAfterVisibleAndEnabled(E);	
	}

	private void addToCurrentTestData(String key, String value) {
		Data.CurrentTestData.put(key, value);
		
	}

	protected void VerifyProductDetails(String expected, String actual) {
	
		SA.assertEquals(actual, expected);

	}
	
	
protected void SelectValueInDropdown(String sKey, SelectDropdown Dropdown) {
		
		String sValue = Data.CurrentTestData.get(sKey);	
		Select dpdn = null;
		
		try {
			 dpdn = (Select) Dropdown.getElement();
		}
		catch(NoSuchElementException E) {
			System.out.println("Dropdown value specified is not present");		
			Data.setCurrentTestDataProperty(sKey, Dropdown.getDefaultValue());
		}
		
		if(dpdn != null) {
			try{
				dpdn.selectByValue(sValue);				
		//		Data.setCurrentTestDataProperty("numberOfQuantity", s);
			}
			catch(NoSuchElementException E) {				
				System.out.println("Dropdown Value specified is not present in the dropdown, Hence selecting the last value instead");	
				int newQu = dpdn.getOptions().size()-1;
				dpdn.selectByIndex(newQu);				
				Data.setCurrentTestDataProperty(sKey, dpdn.getOptions().get(newQu).getText());
		
		//		String s1 = dpdn.getOptions().get(newQu).getText();
			}

		}


	}
	protected void oldSelectValueInDropdown(String s, SelectDropdown qntyDropdown) {
		
		Select dpdn = null;
		try {
			 dpdn = (Select) qntyDropdown.getElement();
		}
		catch(NoSuchElementException E) {
			System.out.println("Quantity dropdown is not present in this product");		
			Data.setCurrentTestDataProperty("numberOfQuantity", "1");
		}
		
		if(dpdn != null) {
			try{
				dpdn.selectByValue(s);				
				Data.setCurrentTestDataProperty("numberOfQuantity", s);
			}
			catch(NoSuchElementException E) {
				System.out.println("Quantity specified is not present in the dropdown and selecting the last value instead");	
				int newQu = dpdn.getOptions().size()-1;
				dpdn.selectByIndex(newQu);
				
				Data.setCurrentTestDataProperty("numberOfQuantity", dpdn.getOptions().get(newQu).getText());
				String s1 = dpdn.getOptions().get(newQu).getText();
			}

		}


	}

	public void ClickOnActionButton(ActionButton addToCartBTN) {
		wait20.until(ExpectedConditions.elementToBeClickable(addToCartBTN.getElement())).click();
		
		
	}

	protected void VerifyOptionalPageLoadText(PageLoadText loadText, String string) {
		try {
			SA.assertEquals(loadText.getElement().toString(), string);
			
		}
		catch(NoSuchElementException e) {
			System.out.println("Optional page is not displayed");
		}

	}

	protected void ClickOnIfExistsActionButton(ActionButton closebutton) {
		try {
			clickAfterVisibleAndEnabled(closebutton.getElement());
		}
		catch(NoSuchElementException e) {
			
		}
	}

	
	protected void CloseBrowser() {
		driver.close();

	}

	private void VerifyDisplayedText(DisplayedText actualText, String expected) {
		SA.assertEquals(actualText, expected);
	}


	public static WebElement findElementByXpath(String xpath) {	
		WebElement	E = driver.findElement(By.xpath(xpath));			
		return E;

	}
	private void clickAfterVisibleAndEnabled(WebElement E) {
		wait20.until(ExpectedConditions.elementToBeClickable(E)).click();
	}



	public static List<WebElement> findElemenstByXpath(String xpath) {
		List<WebElement> E = driver.findElements(By.xpath(xpath));
		return E;
	}
	
	protected void reportsCreateNewTest(String s) {
		Data.currentTest =	Data.report.createTest(s);
	}
	
	protected void reportsCreateNewNode(String s) {
		Data.currentTest.createNode(s);
	}
	
	protected void switchToDefaultContent() {
		driver.switchTo().defaultContent();
	}
	protected  void testClosure() {
		Data.report.flush();
		CloseBrowser();
		SA.assertAll();			
	}
	
	static int i = 0;
	
	public static void clearFilesInScreenshotFolder() {
		  File folder = new File(System.getProperty("user.dir") +  "//Reports//Screenshots");
		  for (File file : folder.listFiles()) {
		   if (file.getName().endsWith(".png")) {
		    file.delete();
		   }
		  }
	}
	public static MediaEntityModelProvider takeNGetScreenshot() {
				MediaEntityModelProvider mediaModel = null ;
				try {
					TakesScreenshot scrShot =((TakesScreenshot)driver);
	                File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
	           
	                String path = System.getProperty("user.dir") + "//Reports//Screenshots//test " + i + ".png";
	                File DestFile=new File(path);
	            
	                Files.copy(SrcFile, DestFile);
					mediaModel =  MediaEntityBuilder.createScreenCaptureFromPath(path).build();
					i = i + 1;
	
				}
				catch(IOException E) {
					E.printStackTrace();
				}
				
            return  mediaModel;
	}

}
