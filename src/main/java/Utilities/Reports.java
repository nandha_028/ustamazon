package Utilities;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports {

	public  ExtentReports extent;
	ExtentHtmlReporter reporter;

	public Reports() {
		extent = new ExtentReports();
		reporter = new ExtentHtmlReporter(".//Reports//TestExecutionReport.html");
		extent.attachReporter(reporter);
	}


	public ExtentTest  getNewTestCase() {
		ExtentTest logger = extent.createTest("LoginTest");
		return logger;
	}


	public void testFlush() {
		extent.flush();
	}

	public ExtentTest getNewNode(ExtentTest test, String S1) {
		ExtentTest node = test.createNode(S1);
		return node;		
	}












}
