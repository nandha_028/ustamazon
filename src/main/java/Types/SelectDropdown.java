package Types;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import Utilities.ActionModule;

public class SelectDropdown {
	String defaultValue;
	Select element;
	 String xpath = "";
	public SelectDropdown(String s, String string) 
	{
		xpath = s;
		defaultValue = string;
	}
	
	public Select getElement() {
		Select E = new Select(ActionModule.findElementByXpath(xpath));
		element = E;

		return element;
	}
	
	public String getDefaultValue() {
		return defaultValue;
	}
}
