package Types;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import Utilities.ActionModule;

public class HeaderTab {
	 WebElement element;
	 String xpath = "";
	public HeaderTab(String s) 
	{
		xpath = s;
	}
	
	public WebElement getElement() {
		WebElement E = ActionModule.findElementByXpath(xpath);
		element = E;
		return element;
	}
}
