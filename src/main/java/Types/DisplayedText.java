package Types;

import org.openqa.selenium.WebElement;

import Utilities.ActionModule;

public class DisplayedText  {
	WebElement element;
	 String xpath = "";
	public DisplayedText(String s) 
	{
		xpath = s;
	}
	
	public WebElement getElement() {
		WebElement E = ActionModule.findElementByXpath(xpath);
		element = E;

		return element;
	}
	
	@Override
	public String toString() {	
		WebElement E = ActionModule.findElementByXpath(xpath);
		element = E;
		return element.getText();
		
	}
	
}
