package Types;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import PageLocators.BestSellerDisplayedProductLocators;

public class BestSellerDisplayedProduct extends BestSellerDisplayedProductLocators {
	public static String Name ;
	public static String Price;
	
	public BestSellerDisplayedProduct(WebElement E) {
		setProperties(E);
		
	}

	private void setProperties(WebElement e) {
		Name = e.findElement(By.xpath(xpName)).getText();
		Price = e.findElement(By.xpath(xpPrice)).getText();
		
	}
}
