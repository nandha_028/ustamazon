package Types;

import java.util.List;

import org.openqa.selenium.WebElement;

import Utilities.ActionModule;

public class ProductList {
	List<WebElement> elements;
	String xpath = "";
	public ProductList(String s) 
	{
		xpath = s;
	}
	
	public List<WebElement> getElementList() {
		List<WebElement> E = ActionModule.findElemenstByXpath(xpath);
		elements = E;
		return  elements;
	}
}
