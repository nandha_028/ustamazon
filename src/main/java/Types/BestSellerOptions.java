package Types;

import org.openqa.selenium.WebElement;

import Utilities.ActionModule;

public class BestSellerOptions {
	private WebElement element;
	 String xpath = "";
	public BestSellerOptions(String s) 
	{
		xpath = s;
	}
	public WebElement getElement() {
		WebElement E = ActionModule.findElementByXpath(xpath);
		element = E ;
		return element;
	}
	
}
