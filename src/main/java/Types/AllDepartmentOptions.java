package Types;

import org.openqa.selenium.WebElement;

import Utilities.ActionModule;

public class AllDepartmentOptions {

	WebElement element;
	 String xpath = "";
	public AllDepartmentOptions(String s) 
	{
		xpath = s;
	}
	
	public WebElement getElement() {
		WebElement E = ActionModule.findElementByXpath(xpath);
		element = E;
		return element;
	}
	
}
