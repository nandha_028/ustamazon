package Types;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import PageLocators.CartItemsLocator;
import Utilities.ActionModule;

public class CartItem extends CartItemsLocator {

	public String itemName;
	public String Totalprice;
		
	public CartItem(WebElement wholeCard) {
		setOtherPropertiesOfCart(wholeCard);
	}
	
	public CartItem() {
		// TODO Auto-generated constructor stub
	}

	private void setOtherPropertiesOfCart(WebElement wholeCard2) {
		itemName = wholeCard2.findElement(By.xpath(xpItemName)).getText();
		Totalprice = wholeCard2.findElement(By.xpath(xpPrice)).getText();
		
	}

	
}
