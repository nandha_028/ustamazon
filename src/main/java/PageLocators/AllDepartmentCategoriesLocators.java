package PageLocators;

public class AllDepartmentCategoriesLocators {	
	
	protected static String xpElectronicsHeader = "//span[contains(text(),'Electronics')][@class='category']";
	protected static String xpCameraNPhones = "//a[contains(text(),'Cameras & Photography')]";
	protected static String xpCameraNPhonesHeader = "//span[contains(text(),'Camera')][@class='category']" ; 
	protected static String xpDigitalCamera = "//a[contains(text(),'Digital Cameras')]";
	protected static String xpDigitalCameraHeader = "//span[contains(text(),'Digital Camera')][@class='category']";
	protected static String xpDisplayedProduct = "//h1[contains(text(),'Bestsellers')]//following::*//li[@class='zg-item-immersion']" ;
	
}
