package PageLocators;

public class AddedToCartLocators {
	protected  static String layout = "//div[@class='a-scroller attach-accessory-section a-scroller-vertical']"; 
	protected  static String xpHeader = "//*[contains(text(),'Added to Cart')]";
	protected  static String xpCartItemCount = "//span[@id='attach-accessory-cart-total-string']";
	protected  static String xpCartSubTotal = "//span[@id='attach-accessory-cart-subtotal']";
	protected  static String xpCloseButton= "//a[@id='attach-close_sideSheet-link']";
	protected static String xpCartButton = "//form[@id='attach-view-cart-button-form']//span[@class='a-button-text'][contains(text(),'Cart')][1]" ;
	

}
