package PageLocators;

public class CartLocators {
	protected static String xpHeader = "//h2[contains(text(),'Shopping Cart')]";
	protected static String xpCartItemList = "//div[@data-name='Active Items']";
	protected static String xpCartItem = "//div[@class='sc-list-item-content']";
	protected static String xpCartItemName = "//span[@class='a-size-medium sc-product-title a-text-bold']";
	protected static String xpSubTotalPrice = "//*[@id='sc-subtotal-amount-activecart']" ;
}
