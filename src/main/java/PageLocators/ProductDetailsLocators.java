package PageLocators;

public class ProductDetailsLocators {
	
	protected static String xpPageLoadText = "//span[contains(text(),'Buy Now')]";
	protected static String xpTitle = "//span[@id='productTitle']";
	protected static String xpFinalPrice = "//span[@id='priceblock_ourprice']";
	protected static String xpQntyDropdown = "//select[@name='quantity']";
	protected static String xpAddToCartBTN = "//span[@id='submit.add-to-cart']//input[@id='add-to-cart-button']";
	

}
