package Page;

import PageLocators.BestSellerLocators;
import Types.BestSellerOptions;
import Types.PageLoadText;

public class BestSeller extends BestSellerLocators {
	
	
	public static PageLoadText HeadLine = new PageLoadText(xpHeadLine);
	public static BestSellerOptions Electronics = new BestSellerOptions(xpElectronicsBSO);
	
	public static final String propHeadLine = "Amazon Bestsellers";
	
	
}
