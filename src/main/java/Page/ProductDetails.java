package Page;

import PageLocators.ProductDetailsLocators;
import Types.ActionButton;
import Types.DisplayedText;
import Types.PageLoadText;
import Types.SelectDropdown;

public class ProductDetails extends ProductDetailsLocators{
	
	public static PageLoadText loadText = new PageLoadText(xpPageLoadText);
	public static  DisplayedText Title = new DisplayedText(xpTitle);
	public static  DisplayedText FinalPrice = new DisplayedText(xpFinalPrice);
	public static  SelectDropdown QntyDropdown = new SelectDropdown(xpQntyDropdown,"1");
	
	public static  ActionButton AddToCartBTN = new ActionButton(xpAddToCartBTN);
	
	
	
}
