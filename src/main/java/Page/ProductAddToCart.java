package Page;

import PageLocators.ProductAddToCartLocators;
import Types.ActionButton;
import Types.PageLoadText;

public class ProductAddToCart extends ProductAddToCartLocators {
	public static PageLoadText loadText = new PageLoadText(xpLoadText);
	public static ActionButton Yes = new  ActionButton(xpYes);
	public static ActionButton NoThanks = new ActionButton(xpNoThanks) ;
}
