package Page;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.Status;

import PageLocators.CartLocators;
import Types.CartItem;
import Types.PageLoadText;
import Utilities.ActionModule;
import Utilities.Data;

@SuppressWarnings("unchecked")
public class Cart extends CartLocators {
	public static PageLoadText Header = new PageLoadText(xpHeader);	
	//	public static CartItem item = new CartItem(xpCartItem);
	private List<WebElement> CartItemnListWE;
	public static List<CartItem> listCItems ;
	static CartItem currentItem = new CartItem();

	public static void setCartItemsList() {
		List<WebElement> listWE = ActionModule.findElemenstByXpath(xpCartItem);				
		List<CartItem> ret = new ArrayList<CartItem>();
		for(WebElement E: listWE) {
			CartItem CI = new CartItem(E);
			ret.add(CI);
		}		
		listCItems = ret;
	}

	public static List<CartItem> getCartItemList(){
		setCartItemsList();
		return listCItems;
	}

	public static void VerifyPresenceOfItem(String string)  {	

		List<CartItem> list = getCartItemList();
		for(CartItem I: list) {
			if(I.itemName.equalsIgnoreCase(string)) {
				currentItem = I;
				Data.currentTest.log(Status.PASS, "Product " + string + " is present in the cart",ActionModule.takeNGetScreenshot());									
			}
			else{
				Data.currentTest.log(Status.FAIL, "Product " + string + " is not present in the cart",ActionModule.takeNGetScreenshot());
			}
		}		
	}

	public static void VerifyPriceSubTotalofItem() {
		String price = Data.CurrentTestData.get("finalPrice");
		String quant = Data.CurrentTestData.get("numberOfQuantity");

		price = price.replace(",","");
		price = price.replace(" " , "");
		price = price.substring(1);

		double pr = Double.parseDouble(price);
		double qn = Double.parseDouble(quant);
		double TotalPrice = pr * qn;

		String actTotalPriceSTR =  ActionModule.findElementByXpath(xpSubTotalPrice).getText();
		actTotalPriceSTR = actTotalPriceSTR.replace(",","");
		actTotalPriceSTR = actTotalPriceSTR.replace("" , "");

		double actTotalPrice = Double.parseDouble(actTotalPriceSTR);
		try {
			if(TotalPrice == actTotalPrice) {            	            
				Data.currentTest.log(Status.PASS, "Sub total value is correct" + "<br>" + "Expected Price :" + TotalPrice + "<br>" +  "Actual Price" + actTotalPrice,ActionModule.takeNGetScreenshot());			
			}
			else {
				Data.currentTest.log(Status.FAIL, "Sub total value is incorrect" + "<br>" + "Expected Price :" + TotalPrice + "<br>" +  "Actual Price" + actTotalPrice,ActionModule.takeNGetScreenshot());
			}
		}
		catch(Exception E) {
			E.printStackTrace();
		}

	}

}
