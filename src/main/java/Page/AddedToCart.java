package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import PageLocators.AddedToCartLocators;
import Types.ActionButton;
import Types.DisplayedText;
import Types.HeaderTab;
import Types.PageLoadText;
import Utilities.ActionModule;

public class AddedToCart extends AddedToCartLocators {
	public static PageLoadText Header = new PageLoadText(xpHeader);
	public static DisplayedText CartItemCount = new DisplayedText(xpCartItemCount);
	public static DisplayedText CartSubTotal = new DisplayedText(xpCartSubTotal);
	public static ActionButton closebutton = new ActionButton(xpCloseButton);
	public static ActionButton cartButton = new ActionButton(xpCartButton);
	
	public static void VerifyAddition() {	
		
		try {
			ActionModule.VerifyPageLoadText(AddedToCart.Header, "Added to Cart");
		}
		catch(TimeoutException E) {
	//		ActionModule.ClickOnActionButton(AddedToCart.cartButton.getElement());	
//			ActionModule.findElementByXpath("//form[@id='attach-view-cart-button-form']//span[@class='a-button-text'][contains(text(),'Cart')][1]").click();
			Actions a = new Actions(ActionModule.driver);
			a.moveToElement(ActionModule.findElementByXpath("//form[@id='attach-view-cart-button-form']//span[@class='a-button-text'][contains(text(),'Cart')][1]")).click();
			a.build().perform();
			
		}
		
		
		
	}
	
}
