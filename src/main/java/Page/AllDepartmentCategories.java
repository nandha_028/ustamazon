package Page;

import PageLocators.AllDepartmentCategoriesLocators;
import Types.AllDepartmentOptions;
import Types.PageLoadText;
import Types.ProductList;

public class AllDepartmentCategories extends AllDepartmentCategoriesLocators {

	
	public static ProductList displayedProducts = new ProductList(xpDisplayedProduct);
	public static PageLoadText ElectronicsHeader = new PageLoadText(xpElectronicsHeader);
	public static AllDepartmentOptions CameraNPhones = new AllDepartmentOptions(xpCameraNPhones);
	public static PageLoadText CameraNPhonesHeader = new PageLoadText(xpCameraNPhonesHeader) ; 
	public static AllDepartmentOptions DigitalCamera = new AllDepartmentOptions(xpDigitalCamera);
	public static PageLoadText DigitalCameraHeader = new PageLoadText(xpDigitalCameraHeader) ; 

}
